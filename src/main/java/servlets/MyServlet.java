package servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class MyServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        try(PrintWriter printWriter = resp.getWriter()) {
            printWriter.println("<!DOCTYPE html>");
            printWriter.println("<html><head>");
            printWriter.println("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'></head>");
            printWriter.println("<title>Hello world!</title>");
            printWriter.println("<body>");
            printWriter.println("<h1>Hello!</h1>");
            printWriter.println("<p>Random number: <strong>" + Math.random() + "</strong></p>");
            printWriter.println("</body></html>");
            req.getRequestDispatcher("/");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/hello.jsp").forward(req,resp);
    }
}
