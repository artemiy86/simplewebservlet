<!DOCTYPE html>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="java.util.*, java.text.*" %>

<%!
    String getFormattedDate(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy hh:mm:ss");
        return simpleDateFormat.format(new Date());
    }
%>
<html>
<body>
<h2>Hello World!</h2>
<i>Today <%= getFormattedDate() %></i>
</body>
</html>
